/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lisen;

/**
 *
 * @author Orlando
 */
public class Lisen{
protected Nodo inicio, fin;   // Punteros para donde esta el inicio y el fin

    public Lisen(){
        inicio = null;
        fin    = null;
       
    }
    
    
    
    
    public void agregarAlInicioint(int elemento){
        inicio = new Nodo(elemento, inicio);
        if (fin == null){
            fin = inicio;
        }
    }
   
    
    
    
    public void mostrarListaEnlazada(){
        Nodo recorrer = inicio;
        System.out.println("");
        while (recorrer != null){
            System.out.print("["+ recorrer.dato+"] -->");
            recorrer = recorrer.siguiente;
        }
        System.out.println("");
    }
    
    
    
    // Metodo para eliminar un nodo del inicio
    public int borrarDelInicio(){
        int elemento = inicio.dato;
        if (inicio == fin){
            inicio = null;
            fin    = null;
        } else {
            inicio = inicio.siguiente;
        }
        return elemento;
    }
    
    
    
    
    public static void main(String[] args) {
          Lisen listaenlazada = new Lisen();
         
        listaenlazada.agregarAlInicioint(20);
        listaenlazada.agregarAlInicioint(20);
        listaenlazada.agregarAlInicioint(20);
        listaenlazada.agregarAlInicioint(20);
        listaenlazada.agregarAlInicioint(20);
        listaenlazada.agregarAlInicioint(20);
        listaenlazada.agregarAlInicioint(20);
        listaenlazada.agregarAlInicioint(20);
        listaenlazada.agregarAlInicioint(20);
        listaenlazada.agregarAlInicioint(20);
        
        listaenlazada.mostrarListaEnlazada();
       
       
        
    }

    
    
     
    
    
    
}
